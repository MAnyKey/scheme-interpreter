module Main 
( main
) where

import System.Environment

import Control.Monad

import LispProcessor.Repl

main :: IO ()
main = do args <- getArgs
          case length args of
              0 -> runRepl
              1 -> runOne args 
              otherwise -> putStrLn "Program takes only 0 or 1 argument"