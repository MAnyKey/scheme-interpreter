module LispProcessor.LispNum
( LispNum(..),
  quotNum,
  remNum,
  modNum,
  integerNum
)where

import Data.Complex
import Data.Ratio

data LispNum =
  Integer Integer
  | Float Double
  | Complex (Complex Double)
  -- | Rational Rational

showNum :: LispNum -> String
showNum (Integer n) = show n
showNum (Float f) = show f
showNum (Complex c) = (show $ realPart c) ++ "+" ++ (show $ imagPart c) ++ "i"
-- showNum (Rational r) = (show $ numerator r) ++ "/" ++ (show $ denominator r)

instance Show LispNum where
  show = showNum


eqNum :: LispNum -> LispNum -> Bool
eqNum (Integer n1) (Integer n2) = fromIntegral n1 == n2
eqNum (Integer n)  (Float f)    = fromIntegral n == f
eqNum (Integer n)  (Complex c)  = fromIntegral n == c
eqNum (Float f1)   (Float f2)   = f1 == f2
eqNum (Float f)    (Complex c)  = (f :+ 0) == c
eqNum (Complex c1) (Complex c2) = c1 == c2
eqNum (Float f)    (Integer n)  = f == fromIntegral n
eqNum (Complex c)  (Float f)    = c == (f :+ 0)
eqNum (Complex c)  (Integer n)  = c == fromIntegral n

instance Eq LispNum where
  (==) = eqNum

plusNum :: LispNum -> LispNum -> LispNum
plusNum (Integer n1) (Integer n2) = Integer $ n1 + n2
plusNum (Integer n)  (Float f)    = Float $ fromIntegral n + f
plusNum (Integer n)  (Complex c)  = Complex $ fromIntegral n + c
plusNum (Float f)    (Integer n)  = Float $ f + fromIntegral n
plusNum (Float f1)   (Float f2)   = Float $ f1 + f2
plusNum (Float f)    (Complex c)  = Complex $ (f :+ 0) + c
plusNum (Complex c1) (Complex c2) = Complex $ c1 + c2
plusNum (Complex c)  (Float f)    = Complex $ c + (f :+ 0)
plusNum (Complex c)  (Integer n)  = Complex $ c + fromIntegral n

minusNum :: LispNum -> LispNum -> LispNum
minusNum (Integer n1) (Integer n2) = Integer $ n1 - n2
minusNum (Integer n)  (Float f)    = Float $ fromIntegral n - f
minusNum (Integer n)  (Complex c)  = Complex $ fromIntegral n - c
minusNum (Float f)    (Integer n)  = Float $ f - fromIntegral n
minusNum (Float f1)   (Float f2)   = Float $ f1 - f2
minusNum (Float f)    (Complex c)  = Complex $ (f :+ 0) - c
minusNum (Complex c1) (Complex c2) = Complex $ c1 - c2
minusNum (Complex c)  (Float f)    = Complex $ c - (f :+ 0)
minusNum (Complex c)  (Integer n)  = Complex $ c - fromIntegral n

mulNum :: LispNum -> LispNum -> LispNum
mulNum (Integer n1) (Integer n2) = Integer $ n1 * n2
mulNum (Integer n)  (Float f)    = Float $ fromIntegral n * f
mulNum (Integer n)  (Complex c)  = Complex $ fromIntegral n * c
mulNum (Float f)    (Integer n)  = Float $ f * fromIntegral n
mulNum (Float f1)   (Float f2)   = Float $ f1 * f2
mulNum (Float f)    (Complex c)  = Complex $ (f :+ 0) * c
mulNum (Complex c1) (Complex c2) = Complex $ c1 * c2
mulNum (Complex c)  (Float f)    = Complex $ c * (f :+ 0)
mulNum (Complex c)  (Integer n)  = Complex $ c * fromIntegral n

negNum :: LispNum -> LispNum
negNum (Integer n) = Integer $ negate n
negNum (Float f) = Float $ negate f
negNum (Complex c) = Complex $ negate c

absNum :: LispNum -> LispNum
absNum (Integer n) = Integer $ abs n
absNum (Float f) = Float $ abs f
absNum (Complex c) = Complex $ abs c

sigNum :: LispNum -> LispNum
sigNum (Integer n) = Integer $ signum n
sigNum (Float f) = Float $ signum f
sigNum (Complex c) = Complex $ signum c


instance Num LispNum where
  (+) = plusNum
  (*) = mulNum
  (-) = minusNum
  negate = negNum
  abs = absNum
  signum = sigNum
  fromInteger = Integer

cmpNum :: LispNum -> LispNum -> Ordering
cmpNum (Integer n1) (Integer n2) = n1 `compare` n2
cmpNum (Integer n)  (Float f)    = fromIntegral n `compare` f
cmpNum (Float f1)   (Float f2)   = f1 `compare` f2
cmpNum (Float f)   (Integer n)   = f `compare` fromIntegral n
-- cmpNum (Integer n)  (Complex c)  = fromIntegral n `compare` c
-- cmpNum (Float f)    (Complex c)  = (f :+ 0) `compare` c
-- cmpNum (Complex c1) (Complex c2) = c1 `compare` c2

instance Ord LispNum where
  compare = cmpNum

toLispComplex :: LispNum -> LispNum
toLispComplex (Integer n) = Complex $ fromIntegral n
toLispComplex (Float f)   = Complex $ (f :+ 0)
toLispComplex val@(Complex _) = val

toLispFloat :: LispNum -> LispNum
toLispFloat (Integer n) = Float $ fromIntegral n
toLispFloat val@(Float _) = val

divNum :: LispNum -> LispNum -> LispNum
divNum (Complex c1)     (Complex c2)  = Complex $ c1 / c2
divNum c@(Complex _)    val           = divNum c  (toLispComplex val)
divNum val              c@(Complex _) = divNum (toLispComplex val) c
divNum (Float f1)       (Float f2)    = Float $ f1 / f2
divNum f@(Float _)      val           = divNum f (toLispFloat val)
divNum val              f@(Float _)   = divNum (toLispFloat val) f
divNum lval@(Integer _) val           = divNum (toLispFloat lval) val


-- divNum (Integer n1) (Integer n2) = Float $ fromIntegral n1 / fromIntegral n2
-- divNum (Integer n)  (Float f)    = Float $ fromIntegral n / f
-- divNum (Integer n)  (Complex c)  = Complex $ fromIntegral n / c
-- divNum (Float f)    (Integer n)  = Float $ f / fromIntegral n
-- divNum (Float f1)   (Float f2)   = Float $ f1 / f2
-- divNum (Float f)    (Complex c)  = Complex $ (f :+ 0) / c
-- divNum (Complex c1) (Complex c2) = Complex $ c1 / c2
-- divNum (Complex c)  (Float f)    = Complex $ c / (f :+ 0)
-- divNum (Complex c)  (Integer n)  = Complex $ c / fromIntegral n

rational2Float :: Rational -> LispNum
rational2Float r = Float $ fromIntegral  (numerator r) / fromIntegral (denominator r)

instance Fractional LispNum where
  (/) = divNum
  fromRational = rational2Float


modNum :: LispNum -> LispNum -> LispNum
modNum (Integer n1) (Integer n2) = Integer $ mod n1 n2

quotNum :: LispNum -> LispNum -> LispNum
quotNum (Integer n1) (Integer n2) = Integer $ quot n1 n2

remNum :: LispNum -> LispNum -> LispNum
remNum (Integer n1) (Integer n2) = Integer $ rem n1 n2

-- instance Integral LispNum where
--   quot = quotNum
--   rem = remNum
--   mod = modNum

integerNum :: LispNum -> Bool
integerNum (Integer _ ) = True
integerNum _            = False