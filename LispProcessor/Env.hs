module LispProcessor.Env
( nullEnv,
  primitiveBindings
) where

import Control.Monad.Error
import System.IO
import Data.IORef

import LispProcessor.Types
import LispProcessor.PrimitiveOps
import LispProcessor.IO
import LispProcessor.Bind

nullEnv :: IO Env
nullEnv = newIORef []

primitiveBindings :: IO Env
primitiveBindings = nullEnv >>= (flip bindVars $ 
                                 map makePrimitiveFunc  primitives
                                 ++ map makeIOFunc ioPrimitives)
    where makePrimitiveFunc = makeFunc PrimitiveFunc
          makeIOFunc = makeFunc IOFunc
          makeFunc contructor (var, func) = (var, contructor func)
