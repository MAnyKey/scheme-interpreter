module LispProcessor.Parser
( readExpr,
  readExprList
) where

import Control.Monad
import Control.Monad.Error
import Numeric
import Text.ParserCombinators.Parsec

import LispProcessor.Types
import LispProcessor.LispNum

symbol :: Parser Char
symbol = oneOf "!$%&|*+-/:<=>?@^_~"

spaces1 :: Parser ()
spaces1 = skipMany1 space

comment :: Parser ()
comment = do
  try $ char ';'
  skipMany $ noneOf "\n"
  (skipMany $ char '\n') <|> eof

comments :: Parser ()
comments = skipMany comment

comments1 :: Parser ()
comments1 = skipMany1 comment

spacesAndComments :: Parser ()
spacesAndComments = skipMany ((space >> return ()) <|> comment)

spacesAndComments1 :: Parser ()
spacesAndComments1 = skipMany1 ((space >> return ()) <|> comment)

readOrThrow :: Parser a -> String -> ThrowsError a
readOrThrow parser input = getLispVal $ parse parser "lisp" input
  where getLispVal (Left err) = throwError $ Parser err
        getLispVal (Right val) = return val 

readExpr = readOrThrow parseExpr
readExprList = readOrThrow (sepEndBy parseExpr spacesAndComments)

        
escapedChars :: Parser Char
escapedChars = do
  char '\\'
  x <- oneOf "\\\"ntr"
  return $ case x of
    '\\' -> x
    '\"' -> x
    'n'  -> '\n'
    't'  -> '\t'
    'r'  -> '\r'

parseString :: Parser LispVal
parseString = do 
  char '"'
  x <- many (escapedChars <|> noneOf "\"\\")
  char '"'
  return $ String x

parseAtom :: Parser LispVal
parseAtom = do
  first <- letter <|> symbol
  rest  <- many ( letter <|> digit <|> symbol <|> char '#') 
  let atom = first:rest
  return $ Atom atom

parseNumber :: Parser LispVal
parseNumber = parseDecimal 
              -- <|> parseExplicitDecimal <|> parseHex <|> parseOct <|> parseBin

parseDecimal :: Parser LispVal
parseDecimal = many1 digit >>= return . Number . Integer . read

parseExplicitDecimal :: Parser LispVal
parseExplicitDecimal = do
  -- try $ string "#d"
  many1 digit >>= return . Number . Integer . read

parseHex :: Parser LispVal
parseHex = do
  -- try $ string "#h"
  many1 hexDigit >>= return . Number . Integer . hexStr2decimal

parseOct :: Parser LispVal
parseOct = do
  -- try $ string "#o"
  many1 octDigit >>= return . Number . Integer . octStr2decimal

parseBin :: Parser LispVal
parseBin = do
  -- try $ string "#b"
  (many1 $ oneOf "01") >>= return . Number . Integer . (binStr2decimal 0)

hexStr2decimal = fst . head . readHex
octStr2decimal = fst . head . readOct

binStr2decimal n "" = n
binStr2decimal n (x:xs) = binStr2decimal (n * 2 + digit) xs
  where digit = if x == '1' then 1 else 0

parseFloat :: Parser LispVal
parseFloat = do
  x <- many1 digit
  char '.'
  y <- many1 digit
  return . Number . Float . fst . head . readFloat $ (x ++ "." ++ y)

parseCharacter :: Parser LispVal
parseCharacter = do
  -- try $ string "#\\"
  val <- try (string "space" <|> string "newline") 
         <|> parseOneChar
  return $ Character $ case val of
    "newline" -> '\n'
    "space"   -> ' '
    otherwise -> head val

parseOneChar :: Parser String
parseOneChar = do
  x <- anyChar
  notFollowedBy alphaNum
  return [x]
  
parseBool :: Parser LispVal
parseBool = do
  try $ char '#'
  oneOf "tf" >>= (return . \val -> case val of
                     't' -> Bool True
                     'f' -> Bool False)

parseHashed :: Parser LispVal
parseHashed = do
  char '#'
  x <- anyChar
  case x of
    't'  -> return $ Bool True
    'f'  -> return $ Bool False
    '\\' -> parseCharacter
    'd'  -> parseExplicitDecimal
    'h'  -> parseHex
    'o'  -> parseOct
    'b'  -> parseBin
    otherwise -> fail $ "unexpected " ++ show x

parseList :: Parser LispVal
parseList = do 
  char '(' >> spaces
  head <- sepEndBy parseExpr spacesAndComments1
  parseListEnd head <|> parseDottedListEnd head

parseListEnd :: [LispVal] -> Parser LispVal
parseListEnd head = spacesAndComments >> char ')' >> (return $ List head)

parseDottedListEnd :: [LispVal] -> Parser LispVal
parseDottedListEnd head = do 
  char '.' >> spacesAndComments1
  tail <- parseExpr
  spacesAndComments >> char ')'
  return $ DottedList head tail

parseQuoted :: Parser LispVal
parseQuoted = do
  char '\''
  x <- parseExpr
  return $ List [Atom "quote", x]

parseQuasiQuoted :: Parser LispVal
parseQuasiQuoted = do
  char '`'
  x <- parseExpr
  return $ List [Atom "quasiquote", x]

parseUnquote :: Parser LispVal
parseUnquote = do 
  char ','
  x <- parseExpr
  return $ List [Atom "unquote", x]

parseExpr :: Parser LispVal
parseExpr = spacesAndComments >> (parseAtom 
            <|> parseString 
            <|> try parseFloat
            <|> parseNumber
            <|> parseQuoted
            <|> parseQuasiQuoted
            <|> parseUnquote
            <|> parseHashed
            <|> parseList)