module LispProcessor.Evaluator 
( eval,
  load,
  apply
) where

import Control.Monad
import Control.Monad.Error
import Data.IORef
import System.IO

import LispProcessor.Types
import LispProcessor.LispNum
import LispProcessor.Vars
import LispProcessor.Parser

eval :: Env -> LispVal -> IOThrowsError LispVal
eval env val@(String _) = return val
eval env val@(Number _) = return val
eval env val@(Bool _) = return val
eval env (Atom id) = getVar env id
eval env val@(Character _) = return val
eval env (List [Atom "quote", val]) = return val 
eval env (List [Atom "if", pred, conseq, alt]) = do
  result <- eval env pred
  case result of
    Bool False -> eval env alt
    otherwise  -> eval env conseq
eval env (List (Atom "cond" : expr : rest)) =
  condEval expr rest
  where condEval (List [cond, value]) (x:xs) = do
          result <- eval env cond
          case result of
            Bool False -> condEval x xs
            otherwise  -> eval env value
        condEval (List [Atom "else", value]) [] = do
          eval env value
        condEval (List [cond, value]) [] = do
          result <- eval env cond
          case result of
            Bool False -> throwError $ Default "Cond variants expired (no one evalutates to #t)"
            otherwise  -> eval env value    
eval env (List [Atom "set!", Atom var, form]) = eval env form >>= setVar env var
eval env (List [Atom "define", Atom var, form]) = eval env form >>= defineVar env var
eval env (List (Atom "define" : List (Atom var : params) : body)) =
  (makeNormalFunc env params body) >>= defineVar env var
eval env (List (Atom "define" : DottedList (Atom var : params) vararg : body)) =
  (makeVarargs vararg env params body) >>= defineVar env var
eval env (List (Atom "lambda" : List params : body)) = makeNormalFunc env params body
eval env (List (Atom "lambda" : DottedList params vararg : body)) =  makeVarargs vararg env params body
eval env (List (Atom "lambda" : vararg@(Atom _) : body)) = makeVarargs vararg env [] body
eval env (List (Atom "let" : argDef : body)) = do
  let (vars, vals) = getLetBindings argDef
  eval env $ let2Lambda vars vals body
  where getLetBindings (List xs) = foldr consArgs ([],[]) xs
        consArgs (List [var, val]) (vars, vals) = (var:vars, val:vals)
        let2Lambda varList valList body = (List (List (Atom "lambda" : List varList : body) : valList))
          
eval env (List [Atom "load", String filename]) = 
    load filename >>= liftM last . mapM (eval env)
eval env (List (function : args)) = do
  func <- eval env function
  argVals <- mapM (eval env) args
  apply func argVals
eval env badForm = throwError $ BadSpecialForm "Unrecognized special form" badForm



apply :: LispVal -> [LispVal] -> IOThrowsError LispVal
apply (PrimitiveFunc func) args = liftThrows $ func args
apply (Func params varargs body closure) args =
  if (num params > num args) 
  then throwError $ NumArgs (num params) args
  else if (num params < num args && varargs == Nothing)
       then throwError $ NumArgs (num params) args
       else (liftIO $ bindVars closure (zip params args)) >>= bindVarArgs varargs >>= evalBody
  where num = toInteger . length
        remainingArgs = drop (length params) args
        evalBody env = liftM last $ mapM (eval env) body
        bindVarArgs (Just arg) env = liftIO $ bindVars env [(arg, List $ remainingArgs)]
        bindVarArgs Nothing env = return env
apply (IOFunc func) args = func args
apply val _ = throwError $ NotFunction "Not a function is provided" val
        
makeFunc varargs env params body = return $ Func (map showVal params) varargs body env

makeNormalFunc :: Monad m => Env -> [LispVal] -> [LispVal] -> m LispVal
makeNormalFunc = makeFunc Nothing
makeVarargs :: Monad m => LispVal -> Env -> [LispVal] -> [LispVal] -> m LispVal
makeVarargs = makeFunc . Just . showVal

load :: String -> IOThrowsError [LispVal]
load filename = (liftIO $ readFile filename) >>= liftThrows . readExprList