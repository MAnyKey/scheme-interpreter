module LispProcessor.Bind
( bindVars
) where

import Control.Monad.Error
import System.IO
import Data.IORef

import LispProcessor.Types

bindVars :: Env -> [(String, LispVal)] -> IO Env
bindVars envRef bindings = readIORef envRef >>= extendEnv bindings >>= newIORef
    where extendEnv bindings env = liftM (++ env) (mapM addBinding bindings)
          addBinding (var, value) = do ref <- newIORef value
                                       return (var, ref)