module LispProcessor.Types
( LispVal(..),
  showVal,
  displayVal,
  
  LispError(..),
  ThrowsError,
  IOThrowsError,
  showError,
  trapError,
  extractValue,
  liftThrows,
  runIOThrows,
  
  Env
)where

import Control.Monad.Error
import Text.ParserCombinators.Parsec
import Data.IORef
import System.IO

import LispProcessor.LispNum

data LispVal = 
  Atom String
  | List [LispVal]
  | DottedList [LispVal] LispVal
  | Number LispNum
  | String String
  | Bool Bool
  | Character Char
  | PrimitiveFunc ([LispVal] -> ThrowsError LispVal)
  | Func {params :: [String], vararg :: (Maybe String), 
          body :: [LispVal], closure :: Env}
  | IOFunc ([LispVal] -> IOThrowsError LispVal)
  | Port Handle

    
instance Show LispVal where 
  show = showVal

unwordsList :: (LispVal -> String) -> [LispVal] -> String
unwordsList func = unwords . map func

escapeChar :: Char -> String
escapeChar '\n' = "newline"
escapeChar '\t' = "\\t"
escapeChar '\r' = "\\r"
escapeChar ' ' = "space"

showVal :: LispVal -> String
showVal (String str) = "\"" ++ str ++ "\""
showVal (Number n) = show n
showVal (Atom name) = name
showVal (Bool True) = "#t"
showVal (Bool False) = "#f"
showVal (Character c) = "#\\" ++ escapeChar c
showVal (List contents) = "(" ++ (unwordsList showVal) contents ++ ")"
showVal (DottedList head tail) = "(" ++ (unwordsList showVal) head ++ " . " ++ showVal tail ++ ")"
showVal (PrimitiveFunc _) = "#<primitive>"
showVal (Func args vararg body env) =
  "#<lambda (" ++ (unwords . (map show) $ args) ++
  (case vararg of
    Nothing -> ""
    Just arg -> " . " ++ show arg) ++ ")>"
showVal (IOFunc _) = "#<IO primitive"
showVal (Port _) = "#<IO port>"

displayVal :: LispVal -> String
displayVal (String str) = str
displayVal (Character c) = [c]
displayVal (List contents) = "(" ++ (unwordsList displayVal) contents ++ ")"
displayVal (DottedList head tail) = "(" ++ (unwordsList displayVal) head ++ " . " ++ displayVal tail ++ ")"
displayVal val = showVal val

--
-- Error
--

data LispError = NumArgs Integer [LispVal]
               | TypeMismatch String LispVal
               | Parser ParseError
               | BadSpecialForm String LispVal
               | NotFunction String LispVal
               | UnboundVar String String
               | Default String

showError :: LispError -> String
showError (UnboundVar message varname) = message ++ ": " ++ varname
showError (BadSpecialForm message form) = message ++ ": " ++ show form
showError (NotFunction message func) = message ++ ": " ++ show func
showError (NumArgs expected found) = "Expected " ++ show expected ++ " args; found values " ++ (unwordsList showVal) found
showError (TypeMismatch expected found) = "Invalid type: expected " ++ expected ++ ", found " ++ show found
showError (Parser parseErr) = "Parse error at " ++ show parseErr
showError (Default message) = "Simple error: " ++ message

instance Show LispError where 
  show = showError

instance Error LispError where
     noMsg = Default "An error has occurred"
     strMsg = Default

type ThrowsError = Either LispError

trapError action = catchError action (return . show)

extractValue :: ThrowsError a -> a
extractValue (Right val) = val

type IOThrowsError = ErrorT LispError IO

liftThrows :: ThrowsError a -> IOThrowsError a
liftThrows (Left err) = throwError err
liftThrows (Right val) = return val

runIOThrows :: IOThrowsError String -> IO String
runIOThrows action = runErrorT (trapError action) >>= return . extractValue

--
-- Env
--

type Env = IORef [(String, IORef LispVal)]